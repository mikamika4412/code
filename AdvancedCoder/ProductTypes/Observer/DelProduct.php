<?php

namespace AdvancedCoder\ProductTypes\Observer;

class DelProduct implements ObserverInterface
{
    protected $request;
    protected $cart;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Checkout\Model\CartFactory $cart
    ) {
        $this->cart = $cart;
        $this->request = $request;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $items = $this->cart->create()->getItems();
        foreach ($items as $item) {
//            if ($item->getProductType() == 'simple')
            if ($item->getId() == 4)
            if ($item->getSku())
//            if ($item->getSku() === '24-MB01')
            {
                $item->isDeleted(true);
            }
        }
    }
}
