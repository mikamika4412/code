<?php
namespace AdvancedCoder\ProductTypes\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class AddProductPlugin implements ObserverInterface
{
    protected $_productRepository;
    protected $_cart;
    protected $formKey;

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
        $this->formKey = $formKey;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $item = $observer->getEvent()->getData('quote_item');
        $product = $observer->getEvent()->getData('product');
        $item = ($item->getParentItem() ? $item->getParentItem() : $item);

        // Enter the id of the prouduct which are required to be added to avoid recurrssion
        if ($item->getSku() === '24-MB01'){
            if ($product->getId() != 4) {
            $params = array(
                'product' => 4,
                'qty' => $product->getQty()
            );
            $_product = $this->_productRepository->getById(4);
            var_dump($_product);
            $this->_cart->addProduct($_product, $params);
            $this->_cart->save();
        }
        }
    }
}
