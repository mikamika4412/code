<?php

namespace AdvancedCoder\ProductTypes\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;


class CustomPrice implements ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //get the item just added to cart
        $item = $observer->getEvent()->getData('quote_item');
        $product = $observer->getEvent()->getData('product');
        //(optional) get the parent item, if exists
        $item = ($item->getParentItem() ? $item->getParentItem() : $item);



        // set your custom price
        if ($item->getSku() === '24-MB05') {
            $price = 0;
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $item->getProduct()->setIsSuperMode(true);
        }




    }
}
//
//use Magento\Framework\Event\ObserverInterface;
//use Magento\Framework\App\RequestInterface;
//
//class AddCharges implements ObserverInterface
//{
//    protected $_productRepository;
//    protected $_cart;
//    protected $formKey;
//
//    public function __construct(\Magento\Catalog\Model\ProductRepository $productRepository, \Magento\Checkout\Model\Cart $cart, \Magento\Framework\Data\Form\FormKey $formKey){
//        $this->_productRepository = $productRepository;
//        $this->_cart = $cart;
//        $this->formKey = $formKey;
//    }
//    public function execute(\Magento\Framework\Event\Observer $observer)
//    {
//        $item = $observer->getEvent()->getData('quote_item');
//        $product = $observer->getEvent()->getData('product');
//        $item = ($item->getParentItem() ? $item->getParentItem() : $item);
//
//        // Enter the id of the prouduct which are required to be added to avoid recurrssion
//        if($product->getSku() != '24-MB05'){
////        if($product->getId() != 2056){
//            $params = array(
//                'SKU' => '24-MB05',
//                'qty' => $product->getQty()
//            );
//            $_product = $this->_productRepository->getSku('24-MB05');
//            $this->_cart->addProduct($_product,$params);
//            $this->_cart->save();
//        }
//
//    }
//}
