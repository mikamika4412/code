<?php

namespace AdvancedCoder\ProductTypes\Plugin;

class AddProductPlugin extends \Magento\Framework\View\Element\Template
{

    protected $_productRepository;
    private array $listSKURelations = ['24-MB03' => ["24-MB05"], 'sku2' => ["sku2_1, sku2_2"]];

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        array $data = []
    )
    {
        $this->_productRepository = $productRepository;
        parent::__construct($context, $data);
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

    public function addProduct($productInfo, $requestInfo = null)
    {
        $product = $this->_getProduct($productInfo);
        $productId = $product->getId();

        if ($productId) {
            $request = $this->getQtyRequest($product, $requestInfo);
            try {
                $this->_eventManager->dispatch(
                    'checkout_cart_product_add_before',
                    ['info' => $requestInfo, 'product' => $product]
                );
                $result = $this->getQuote()->addProduct($product, $request);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->_checkoutSession->setUseNotice(false);
                $result = $e->getMessage();
            }

            $this->addSKUrelatedProductIfPresent($product->getSku());
            /**
             * String we can get if prepare process has error
             */
            if (is_string($result)) {
                if ($product->hasOptionsValidationFail()) {
                    $redirectUrl = $product->getUrlModel()->getUrl(
                        $product,
                        ['_query' => ['startcustomization' => 1]]
                    );
                } else {
                    $redirectUrl = $product->getProductUrl();
                }
                $this->_checkoutSession->setRedirectUrl($redirectUrl);
                if ($this->_checkoutSession->getUseNotice() === null) {
                    $this->_checkoutSession->setUseNotice(true);
                }
                throw new \Magento\Framework\Exception\LocalizedException(__($result));
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__('The product does not exist.'));
        }

        $this->_eventManager->dispatch(
            'checkout_cart_product_add_after',
            ['quote_item' => $result, 'product' => $product]
        );
        $this->_checkoutSession->setLastAddedProductId($productId);
        return $this;
    }


    private function addSKUrelatedProductIfPresent($productSku) {
        $skuChield = $this->hasSKURelation($productSku);
        if ($skuChield && $this->isNotPresentedInCard($skuChield)) {
            $this->addProduct($this->getProductBySku($productSku));
        }
    }

    private function isNotPresentedInCard($productSku) {
        //TODO verify that product is not already in card
    }

    private function hasSKURelation($productSku):string {
        for($i = 0; $i < count($this->listSKURelations); $i++) {
            if ($productSku === $this->listSKURelations[$i]) {
                return $i;
            }
        }
        return false;
    }
}
