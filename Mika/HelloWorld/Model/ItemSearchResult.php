<?php

namespace Mika\HelloWorld\Model;

use Magento\Framework\Api\SearchResults;
use Mika\HelloWord\Api\ItemSearchResultInterface;

class ItemSearchResult extends SearchResults implements ItemSearchResultInterface
{
}
