<?php

namespace Mika\HelloWorld\Cron;

use Mika\HelloWorld\Model\ItemFactory;

/**
 * Adding deadlines to the schedule
 */
class AddItem
{
    /**
     * @var $itemFactory Mika\HelloWorld\Model\ItemFactory;
     */
    private Mika\HelloWorld\Model\ItemFactory $itemFactory;

    /**
     * @param \Mika\HelloWorld\Model\ItemFactory $itemFactory
     */
    public function __construct(ItemFactory $itemFactory)
    {
        $this->itemFactory = $itemFactory;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->itemFactory->create()
            ->setNews('Scheduled item1' . time())
            ->setCommet('Created at' . time())
            ->save();
    }
}

