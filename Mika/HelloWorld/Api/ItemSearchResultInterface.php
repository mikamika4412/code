<?php

namespace Mika\HelloWord\Api;

interface ItemSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * @return \Mika\HelloWord\Api\Data\ItemInterface[]
     */
    public function getItems();

    /**
     * @param \Mika\HelloWord\Api\Data\ItemInterface[]
     * @return void
     */
    public function setItems(array $items);
}
