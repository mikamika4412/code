<?php

namespace Mika\HelloWorld\Console\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mika\HelloWorld\Model\ItemFactory;
use Magento\Framework\Console\Cli;

/**
 * Class AddItem  Console
 */
class AddItem extends Command
{
    /**
     * constants with fields of the table in which we write data
     */
    public const INPUT_KEY_NEWS = 'news';
    public const INPUT_KEY_COMMENT = 'comment';
    public const INPUT_KEY_VALUE = 'value';
    /**
     * @var $itemFactory
     */
    private ItemFactory $itemFactory;


    /**
     * @param \Mika\HelloWorld\Model\ItemFactory $itemFactory
     */
    public function __construct(ItemFactory $itemFactory)
    {
        $this->itemFactory = $itemFactory;
        parent::__construct();
    }


    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName('mika:item:add')
            ->addArgument(
                self::INPUT_KEY_NEWS,
                InputArgument::REQUIRED,
                'ItemName'
            )->addArgument(
                self::INPUT_KEY_COMMENT,
                InputArgument::OPTIONAL,
                'Comment'
            )->addArgument(
                self::INPUT_KEY_VALUE,
                InputArgument::OPTIONAL,
                'ItemValue'
            );
        parent::configure();
    }


    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $item = $this->itemFactory->create();
        $item->setNews($input->getArgument(self::INPUT_KEY_NEWS));
        $item->setComment($input->getArgument(self::INPUT_KEY_COMMENT));
        $item->setValue($input->getArgument(self::INPUT_KEY_VALUE));
//        $item->setIsObjectNew(true); // check if you need it?
        $item->save();
        return Cli::RETURN_SUCCESS;
    }
}

