<?php

namespace Mika\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Table mika_news FrontEnd
 */
class Index_1 extends \Magento\Framework\App\Action\Action
{
public function __construct(Context $context)
{
    parent::__construct($context);
}

    /**
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
//        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE, array("helloworld"));
//        return $result;
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);

//        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
//        $result->setContents('Hello World');
//        return $result;
    }
}
