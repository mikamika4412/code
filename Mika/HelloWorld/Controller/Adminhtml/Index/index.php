<?php


namespace Mika\HelloWorld\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Index class Hello Admins in BackEnd
 */
class Index extends \Magento\Backend\App\Action
{

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    )
    {
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
//        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
//        $result->setContents('Hello Admins');
//        return $result;
//        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);


$resultPage = $this->pageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('bla')));

        return $resultPage;

    }
}
