<?php

namespace Mika\HelloWorld\Controller\Adminhtml\Hello\World;

use Mika\HelloWorld\Api\Data\HelloWorldInterface;
use Mika\HelloWorld\Api\HelloWorldRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends Action implements HttpPostActionInterface
{

    private HelloWorldRepositoryInterface $productTypesRepository;

    public function __construct(
        Context $context,
        HelloWorldRepositoryInterface $productTypesRepository
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
    }

    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $productTypeId = (int)$this->getRequest()->getParam('id');

        if(!$productTypeId) {
            $this->messageManager->addErrorMessage(__('Error.'));
            return $resultRedirect->setPath('*/*/index');

        }

        try {
            $productType = $this->productTypesRepository->get($productTypeId);
            $this->productTypesRepository->delete($productType);
            $this->messageManager->addSuccessMessage(__('You deleted the product type.'));

        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('Cannot delete product type'));

        }
        return $resultRedirect->setPath('*/*/index');
    }
}
