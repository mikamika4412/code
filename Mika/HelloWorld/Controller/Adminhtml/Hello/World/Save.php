<?php

namespace Mika\HelloWorld\Controller\Adminhtml\Hello\World;

use Mika\HelloWorld\Api\HelloWorldRepositoryInterface;
use AdvancedCoder\ProductTypes\Model\HelloWorldFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use AdvancedCoder\ProductTypes\Api\Data\ProductTypesInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceInterface;

class Save extends Action implements HttpPostActionInterface
{
    private HelloWorldRepositoryInterface $productTypesRepository;
    private HelloWorldFactory $productTypesFactory;

    public function __construct(
        Context $context,
        HelloWorldRepositoryInterface $productTypesRepository,
        HelloWorldFactory $productTypesFactory
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
        $this->productTypesFactory = $productTypesFactory;
    }

    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $requestData = $request->getPost()->toArray();

        if (!$request->isPost() || empty($requestData['general'])) {
            $this->messageManager->addErrorMessage(__('Wrong request.'));
            $resultRedirect->setPath('*/*/new');
            return $resultRedirect;
        }

        try {
            $id = $requestData['general'][HelloWorldInterface::ID];
            $productType = $this->productTypesRepository->get($id);
        } catch (\Exception $e) {
            $productType = $this->productTypesFactory->create();
        }

        $productType->setType($requestData['general'][HelloWorldInterface::TYPE]);
        try {
            $productType = $this->productTypesRepository->save($productType);
            $this->processRedirectAfterSuccessSave($resultRedirect, $productType->getId());
            $this->messageManager->addSuccessMessage(__('Product type was saved.'));

        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error. Cannot save'));

            $resultRedirect->setPath('*/*/new');
        }

        return $resultRedirect;
    }

    private function processRedirectAfterSuccessSave(Redirect $resultRedirect, string $id)
    {
        if ($this->getRequest()->getParam('back')) {
            $resultRedirect->setPath(
                '*/*/edit',
                [
                    HelloWorldInterface::ID => $id,
                    '_current' => true,
                ]
            );
        } elseif ($this->getRequest()->getParam('redirect_to_new')) {
            $resultRedirect->setPath(
                '*/*/new',
                [
                    '_current' => true,
                ]
            );
        } else {
            $resultRedirect->setPath('*/*/');
        }
    }
}
