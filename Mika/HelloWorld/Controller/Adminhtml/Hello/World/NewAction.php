<?php

namespace Mika\HelloWorld\Controller\Adminhtml\Hello\World;

use Mika\HelloWorld\Model\HelloWorldRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceInterface;
use Mika\HelloWorld\Api\Data\HelloWorldInterface;

class NewAction extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Advanced_Coder::product_types';
    private HelloWorldRepository $productTypesRepository;

    public function __construct(
        Context $context,
        HelloWorldRepository $productTypesRepository
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
    }


    public function execute(): ResultInterface
    {
        /** @var Page $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $result->setActiveMenu('Mika_HelloWorld::mika')
            ->addBreadcrumb(__('New news'), __('Hello World'));
        $result->getConfig()->getTitle()->prepend(__('New news'));

        return $result;
    }
}
