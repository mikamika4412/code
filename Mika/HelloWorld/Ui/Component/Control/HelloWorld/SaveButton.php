<?php

//namespace AdvancedCoder\ProductTypes\Ui\Component\Control\ProductType;
namespace Mika\HelloWorld\Ui\Component\Control\HelloWorld;

use AdvancedCoder\ProductTypes\Ui\Component\Control\ProductType\GenericButton;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton extends GenericButton implements ButtonProviderInterface
{
    public function getButtonData()
    {
        return [
            'label' => __('Save Product Type'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 20,
        ];
    }
}
